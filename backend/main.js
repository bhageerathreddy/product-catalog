const express = require('express')
const app = express()
const productsRouter = require('./routes/products')

require('dotenv').config()

app.use('/api/v1/products', productsRouter)

app.get('*', (req, res) => {
    res.status(404).send("Please check the URL and try again")
})

app.listen(8410)
