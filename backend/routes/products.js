const express = require('express')
const router = express.Router()
const axios = require('axios')

// we are not using this
async function listCategories(callback) {
    let query = {
        "size": 0,
        "aggs": {
            "categories": {
                "terms": {
                    "field": "category",
                    "order": {
                        "_count": "desc"
                    }
                }
            }
        }
    }

    return axios.post(process.env.ES_ENDPOINT + '/products/_search', query, {
        auth: {
            username: process.env.ES_USERNAME,
            password: process.env.ES_PASSWORD
        }
    }).then((response) => callback(response))
        .catch((error) => callback(null, error))
}

// we are not using this
async function listSubCategories(callback) {
    let query = {
        "size": 0,
        "aggs": {
            "sub_categories": {
                "terms": {
                    "field": "sub_category",
                    "order": {
                        "_count": "desc"
                    }
                }
            }
        }
    }

    return axios.post(process.env.ES_ENDPOINT + '/products/_search', query, {
        auth: {
            username: process.env.ES_USERNAME,
            password: process.env.ES_PASSWORD
        }
    }).then((response) => callback(response))
        .catch((error) => callback(null, error))
}

async function searchProducts(searchTerm, from, size, reducedInfo, callback) {

    if (!from) from = 0
    if (!size) size = 100
    if (size > 10000) size = 10000

    // Search product titles only
    let query = {
        "from": from,
        "size": size,
        "query": {
            "match": {
                "title": {
                    "query": searchTerm,
                    "analyzer": "standard"
                }
            }
        }
    }

    if (reducedInfo) {
        query["_source"] =[
            "title",
            "description",
            "pid"
        ]
    }

    return axios.post(process.env.ES_ENDPOINT + '/products/_search', query, {
        auth: {
            username: process.env.ES_USERNAME,
            password: process.env.ES_PASSWORD
        }
    }).then((response) => callback(response))
        .catch((error) => callback(null, error))
}

router.get('/categories', (req, res) => {

    listCategories((response, error) => {
        if (error != null) {
            console.log(error)
            res.status(500).send("Failed to list categories")
            return
        }

        response = response.data
        result = []
        if (response.aggregations.categories.buckets) {
            for (var i in response.aggregations.categories.buckets) {
                result.push({
                    key: response.aggregations.categories.buckets[i].key,
                    count: response.aggregations.categories.buckets[i].doc_count
                })
            }
        }

        res.json(result)
    })
})

router.get('/sub-categories', (req, res) => {

    listSubCategories((response, error) => {
        if (error != null) {
            console.log(error)
            res.status(500).send("Failed to list sub categories")
            return
        }

        response = response.data
        result = []
        if (response.aggregations.sub_categories.buckets) {
            for (var i in response.aggregations.sub_categories.buckets) {
                result.push({
                    key: response.aggregations.sub_categories.buckets[i].key,
                    count: response.aggregations.sub_categories.buckets[i].doc_count
                })
            }
        }

        res.json(result)
    })
})

// We would need a dedicated API for auto complete but in interest of time, just using the same API
// for autocomplete as well as product catalog
router.get('/search', (req, res) => {

    if (req.query.q) {
        searchProducts(req.query.q, req.query.from, req.query.size, req.query.ac, (response, error) => {
            if (error != null) {
                console.log(error)
                res.status(500).send("Failed to query the search term")
                return
            }
            
            // Dataset has records with duplicate titles, so adding this to make sure only unique ones show up in UI
            uniqueProducts = new Set()
            result = []
            if (response.data.hits.hits) {
                response = response.data.hits.hits
                for (var i in response) {
                    if (uniqueProducts.has(response[i]._source.title))
                        continue

                    uniqueProducts.add(response[i]._source.title)
                    if (req.query.ac) { // for auto-complete, reduce the info in response
                        result.push({
                            title: response[i]._source.title,
                            description: response[i]._source.description,
                            product_id: response[i]._source.pid
                        })
                    }
                    else {
                        result.push({
                            title: response[i]._source.title,
                            description: response[i]._source.description,
                            product_id: response[i]._source.pid,
                            price: response[i]._source.selling_price,
                            outOfStock: response[i]._source.out_of_stock,
                            rating: response[i]._source.average_rating,
                            url: response[i]._source.url,
                            imageUrl: (response[i]._source.images instanceof Array) ? response[i]._source.images[0] : "",
                        })
                    }
                }

                // return only top 10 results
                // TODO: make this configurable instead of hard coding?
                if (req.query.ac)
                    result.slice(0, 10)
            }

            console.log(result)

            res.json(result)
        })
    }
    else {
        res.status(400).send('Request is missing search query')
    }
})

module.exports = router
