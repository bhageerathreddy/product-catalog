const fs = require('fs')
const axios = require('axios')

// Simple nodeJS to import the flipkart dataset JSON into the local elastic search instance
// elasticdump is not able to import the JSON directly
// There is some issue with _id and product_details fields of each record, remove them and
// push it into the index
//
// Dataset secured from - https://data.world/opensnippets/flipkart-fashion-products-dataset/workspace/file?filename=flipkart_fashion_products_dataset.json
//
// Query to create the index on Elastic Search -
// PUT /products?pretty
// {
//     "settings" : {
//         "number_of_shards" : 1,
//         "number_of_replicas" : 1,
//         "analysis": {
//           "filter": {
//             "autocomplete_filter": {
//               "type": "edge_ngram",
//               "min_gram": 1,
//               "max_gram": 20
//             }
//           },
//           "analyzer": {
//             "autocomplete": {
//               "type": "custom",
//               "tokenizer": "standard",
//               "filter": [
//                 "lowercase",
//                 "autocomplete_filter"
//               ]
//             }
//           }
//         }
//     },
//     "mappings" : {
//         "properties" : {
//             "actual_price" : { "type" : "text" },
//             "average_rating" : { "type" : "float" },
//             "brand" : { "type" : "text" },
//             "category" : { "type" : "keyword" },
//             "description" : { "type" : "text", "analyzer": "autocomplete" },
//             "discount" : { "type" : "text" },
//             "images" : { "type" : "text" },
//             "out_of_stock" : { "type" : "boolean" },
//             "pid" : { "type" : "keyword" },
//             "seller" : { "type" : "text" },
//             "selling_price" : { "type" : "text" },
//             "sub_category" : { "type" : "keyword" },
//             "title" : { "type" : "text", "analyzer": "autocomplete" },
//             "url" : { "type" : "text" }
//         }
//     }
// }

async function postEntry(entry) {

  return axios.post('http://localhost:9200/products/_doc', entry, {
    auth: {
      username: 'elastic',
      password: 'BRfM2S5-S0O6TX*15hT7'
    }
  }).then((response) => (response.status))
    .catch((err) => { console.log(err.response.data) })
}

fs.readFile('flipkart_fashion_products_dataset.json', 'utf8', async (err, data) => {
  if (err) {
      console.error(err)
      return
  }

  original = JSON.parse(data)
  for (var key in original) {

    delete original[key]._id
    delete original[key].product_details

    process.stdout.write("\rPushing Record " + (key + 1))
    const resp = await postEntry(original[key])
  }

  console.log("\nImport Complete")
})
