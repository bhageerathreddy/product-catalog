import { Autocomplete, Button, Divider, Stack, TextField } from '@mui/material'
import React, { useState, useRef } from 'react'

function SearchBox({ setSearchQuery }) {

    const [searchResults, setSearchResults] = useState([])
    const [searchString, setSearchString] = useState("")
    const previousController = useRef()

    const getSearchResults = (searchTerm) => {

        // Abort any previous requests for search as they are not required
        if (previousController.current) {
            previousController.current.abort()
        }

        const controller = new AbortController()
        const signal = controller.signal
        previousController.current = controller

        fetch("/api/v1/products/search?ac=true&q=" + searchTerm, {
            signal,
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json"
            }
        })
        .then(response => response.json())
        .then(results => setSearchResults(results.map(oneResult => { return { title: oneResult.title }})))
        .catch(error => {}) // catch DOMExceptions for Aborted Requests
    }

    const onInputChange = (event, searchTerm, reason) => (searchTerm === "") ? setSearchResults([]) : getSearchResults(searchTerm)

    return (
        <Stack
            sx={{ width: 500, margin: "auto", paddingTop: "50px" }}
            direction="row"
            spacing={2}
            justifyContent="center"
            alignItems="center"
        >
            <Autocomplete
                freeSolo
                id="search-box"
                options={searchResults}
                onInputChange={onInputChange}
                getOptionLabel={(option) => (typeof option === 'string' || option instanceof String) ? option : option.title }
                style={{ width: 400 }}
                noOptionsText={"No Products Found"}
                isOptionEqualToValue={(option, value) => option.title === value.title }
                renderInput={(params) => (
                    <TextField
                        {...params}
                        id="search-box-text"
                        label="Search Products"
                        variant="outlined"
                        value={searchString}
                        onChange={e => setSearchString(e.target.value)}
                        onKeyDown={e => { if (e.code.toUpperCase() === 'ENTER') setSearchQuery(searchString) }}
                    />
                )}
            />

            <Button
                sx={{ width: 50, pl: 5, pr: 5 }}
                disableElevation
                size="large"
                variant="contained"
                onClick={() => setSearchQuery(searchString)}
            >
                Search
            </Button>
        </Stack>
    )
}

export default SearchBox