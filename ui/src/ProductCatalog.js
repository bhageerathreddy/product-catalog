import { Grid } from '@mui/material'
import React, { useEffect, useRef, useState } from 'react'
import ProductContainer from './ProductContainer'

const ProductCatalog = ({ searchQuery }) => {
    const [paginationState, setPaginationState] = useState({ from: 0, size: 48})
    const [searchResults, setSearchResults] = useState([])
    const previousController = useRef()

    useEffect(() => {
        if (searchQuery === "")
            return
        
        // Abort any previous requests for search as they are not required
        if (previousController.current) {
            previousController.current.abort()
        }

        const controller = new AbortController()
        const signal = controller.signal
        previousController.current = controller

        fetch("/api/v1/products/search?q=" + searchQuery + "&from=" + paginationState.from + "&size=" + paginationState.size, {
            signal,
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json"
            }
        })
        .then(response => {
            return response.json()
        })
        .then(results => {
            setSearchResults(results)
        })
        .catch(_ => {}) // catch DOMExceptions for Aborted Requests

    }, [searchQuery])
    
    return (
        <Grid sx={{ mb: 5, pt: 5 }} container spacing={2}>
            {searchResults.map(product => {
                return (
                    <ProductContainer title={product.title} description={product.description} key={product.title} imageUrl={product.imageUrl}
                        price={product.price} outOfStock={product.outOfStock} rating={product.rating === "" ? 0: parseFloat(product.rating)} url={product.url}
                    />
                )
            })}
        </Grid>
    )
}

export default ProductCatalog