import { Grid, Link, Rating, Typography } from '@mui/material'
import { Box } from '@mui/system'
import React from 'react'

const ProductContainer = ({ title, description, price, outOfStock, rating, url, imageUrl }) => {
    return (
        <Grid item md={2}>
            <Box
                sx={{ width: "100%", height: 500 }}
                component="img"
                src={imageUrl}
            />

            <Typography variant="h6" sx={{ fontWeight: "bold" }}>
                <Link href={url} underline="always">
                    {title}
                </Link>
            </Typography>

            <Typography variant="body2">
                {description}
            </Typography>

            <Typography variant="h5" sx={{ fontWeight: "bold" }}>
                ₹{price}
            </Typography>

            <Typography variant="subtitle2" sx={{ color: "gray" }}>
                {outOfStock ? "Out of Stock" : "In Stock"}
            </Typography>

            <Box sx={{ display: "flex", alignItems: "center" }}>
                <Rating value={rating} precision={0.1} readOnly />
                <Typography>{rating}</Typography>
            </Box>
        </Grid>
    )
}

export default ProductContainer