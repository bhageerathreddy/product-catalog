import { useState, useEffect } from "react"
import ProductCatalog from "./ProductCatalog"
import SearchBox from "./SearchBox"

function App() {
  const [searchQuery, setSearchQuery] = useState("")

  useEffect(() => {
    // console.log("Searching for", searchQuery)
  }, [searchQuery])

  return (
    <div>
      <SearchBox setSearchQuery={ setSearchQuery } />
      <ProductCatalog searchQuery={ searchQuery } />
    </div>
  )
}

export default App
